Shelf Falling Project

Description: A shelf was designed such that when the countdown timer expires, an Attiny85 initiates the 'shelf falling' action of the artwork. 
This action was implemented using a high torque motor pulling a pin holds the platform in place. 
Two Adafruit 7-segment LED backpacks were used to showcase the days, hours, minutes, and seconds within the countdown using I2C.  

[Circuit Design](https://upverter.com/jhlink/83f6e9718ba8f405/Shelf-Controller-Board-V2/)