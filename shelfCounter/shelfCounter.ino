/* SevenSegTest_tiny
   It requires the TinyWireM library for I2C communication
   and the Tiny_LEDBackpack library for the various display
   functions.  
   TinyWireM library obtained from http://www.scenelight.nl/?wpfb_dl=22

NOTE: Tested successfully on ATtiny85, using 4700ohm pull-up
resistors on SCL and SDA lines. Tested using 1MHz internal clock.
ATtiny85 physical pin7 --> SCL pin on LED Backpack
ATtiny85 physical pin5 --> SDA pin on LED Backpack

The attiny85 can simulate I2C on PB2 (pin 7) (SCL) and PB0 (pin 5) (SDA). 

Note: ATtiny85 works fine with no pullup resistors. 
The following code was flashed with vanilla LED Backpack and Adafruit Trinket 8 Mhz

Also tested with 10k ohm pull-up resistors on SDA and SCL lines.
 */

#define MAJOR 0 
#define MINOR 1
#define PATCH 2

#include <TinyWireM.h>
#include "Adafruit_LEDBackpack.h" // Modified version of LEDBackpack.
#include <RBD_Timer.h> // https://github.com/alextaujenis/RBD_Timer

Adafruit_7segment minSecSeg = Adafruit_7segment();
Adafruit_7segment dayHourSeg = Adafruit_7segment();

const long i2c_add_min_sec_min_sec = 0x70; // stock address for Adafruit 7-segment LED backpack
const long i2c_add_min_sec_day_hr = 0x71; // stock address for Adafruit 7-segment LED backpack

#define solenoidPin 3

//	Change this value (in seconds) for a different countdown time 
const int days = 0;
const int hours = 0;
const int minutes = 11;
const int seconds = 0;

//	Change this value (in seconds) to turn on the solenoid/motor for this amount of time
long solenoidTimeOut = 10;

RBD::Timer timer;

struct Countdown {
  int tempDays;
  int tempHours;
  int tempMinutes;
  int tempSeconds;
};

bool goBackInTime = false;

Countdown mutableCountDown;

void setup() {

  initializeCountDown(mutableCountDown);

  pinMode(solenoidPin, OUTPUT);
  digitalWrite(solenoidPin, LOW); 

  initializeSegment(minSecSeg, i2c_add_min_sec_min_sec); 
  initializeSegment(dayHourSeg, i2c_add_min_sec_day_hr); 

  delay(1000);

  //  Timer timeout accepts parameter in ms.
  timer.setTimeout(1000);
  timer.restart();
}

void loop() {
  if (timer.onExpired()) {
    if (goBackInTime) {
      incrementSeconds(); 
    } else {
      decrementSeconds();
    }
    timer.restart();
  }
}

void initializeCountDown(Countdown& inputCountDown) {
  inputCountDown.tempDays = days;
  inputCountDown.tempHours = hours;
  inputCountDown.tempMinutes = minutes;
  inputCountDown.tempSeconds = seconds;
}

void incrementSeconds() {
  if (mutableCountDown.tempSeconds < 59) {
    mutableCountDown.tempSeconds++;
    
    updateDigitSegments();
  } else {
    carryTimeDigit(0); 

    updateDigitSegments();
  }
}

void carryTimeDigit(int carryOverDigit) {

  //  If 0, the seconds place requires a carry over. 
  //  If 1, the minutes place requires a carry over. 
  //  If 2, the hours place requires a carry over. 

  switch(carryOverDigit) {
    case 0:
      if (mutableCountDown.tempMinutes < 59) {
        mutableCountDown.tempMinutes++;
        mutableCountDown.tempSeconds = 0;
        break;
      }

    case 1:
      if (mutableCountDown.tempHours < 23) {
        mutableCountDown.tempHours++;
        mutableCountDown.tempMinutes = 0;

        carryTimeDigit(0);
        break;
      }

    case 2:
      mutableCountDown.tempDays++;
      mutableCountDown.tempHours = 0;

      carryTimeDigit(1);
      break;
    }
}

void decrementSeconds() {
  if (mutableCountDown.tempSeconds > 0) {
    mutableCountDown.tempSeconds--;
    
    updateDigitSegments();
  } else if (checkForRemainingTime()) {
    borrowTimeDigit(0); 

    updateDigitSegments();
  } else {
    turnOnSolenoid();
    goBackInTime = true;
  }
}  

void borrowTimeDigit(int borrowOverDigit) {

  //  If 0, the seconds place requires a carry over. 
  //  If 1, the minutes place requires a carry over. 
  //  If 2, the hours place requires a carry over. 

  switch(borrowOverDigit) {
    case 0:
      if (mutableCountDown.tempMinutes > 0) {
        mutableCountDown.tempMinutes--;
        mutableCountDown.tempSeconds = 59;
        break;
      }

    case 1:
      if (mutableCountDown.tempHours > 0) {
        mutableCountDown.tempHours--;
        mutableCountDown.tempMinutes = 59;

        borrowTimeDigit(0);
        break;
      }

    case 2:
      if (mutableCountDown.tempDays > 0) {
        mutableCountDown.tempDays--;
        mutableCountDown.tempHours = 23;

        borrowTimeDigit(1);
        break;
      }
    }
}

bool checkForRemainingTime() {
  if (mutableCountDown.tempDays > 0) {
    return true;
  } else if (mutableCountDown.tempHours > 0) {
    return true;
  } else if (mutableCountDown.tempMinutes > 0) {
    return true;
  } else if (mutableCountDown.tempSeconds > 0) {
    return true;
  } else {
    return false;
  }
}
  
void initializeSegment(Adafruit_7segment& inputSegment, long i2c_add_min_sec) {
  inputSegment.begin(i2c_add_min_sec); // initialize HT16K33 controller
  inputSegment.blinkRate(0); // set blink rate (0,1,2 or 3)
  inputSegment.clear(); // clear all digits on display
  inputSegment.writeDisplay(); // push data to display
}

void turnOnSolenoid() {
  timer.stop();
  digitalWrite(solenoidPin, HIGH);
  delay(solenoidTimeOut * 1000);
  digitalWrite(solenoidPin, LOW);
}

void updateDigitSegments() {
  writeDoubleDigit(dayHourSeg, mutableCountDown.tempDays, 0);
  writeDoubleDigit(dayHourSeg, mutableCountDown.tempHours, 3);
  writeDoubleDigit(minSecSeg, mutableCountDown.tempMinutes, 0);
  writeDoubleDigit(minSecSeg, mutableCountDown.tempSeconds, 3);

  dayHourSeg.writeDisplay();    // push data to display
  minSecSeg.writeDisplay();    
}

void writeDoubleDigit(Adafruit_7segment& inputSegment, int valueToWrite, int startingDigitPair) {
  // Example of writing a single digit to a specified position on the
  // display. The 1st argument is the digit position (0,1,3,4), 2nd
  // value is the numeric value to display (0-9, a-F), and the 3rd 
  // value is whether to show the decimal point. Note that if you
  // don't clear the display ahead of time, any previously displayed 
  // values in other digits will remain visible. Digit position 2 is
  // the colon, so we don't try to write numbers to it. 

  int onesDigitValue = valueToWrite % 10;
  int tensDigitValue = valueToWrite / 10;


  int onesDigitIndex = 0;
  int tensDigitIndex = 0;

  if (startingDigitPair == 0) {
    onesDigitIndex = 1;
    tensDigitIndex = 0;
  } else if (startingDigitPair == 3) {
    onesDigitIndex = 4;
    tensDigitIndex = 3;
  }

  if (tensDigitValue != 0) {
    inputSegment.writeDigitNum(tensDigitIndex, tensDigitValue, false);
  } else {
    inputSegment.writeDigitRaw(tensDigitIndex, 0x00);
  }

  inputSegment.writeDigitNum(onesDigitIndex, onesDigitValue, false);
}

void clearSegments(Adafruit_7segment& inputSegment) {
  inputSegment.clear();
  inputSegment.writeDisplay();
}
